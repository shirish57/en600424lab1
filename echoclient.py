from twisted.internet.protocol import Protocol, ClientFactory
from twisted.internet.endpoints import TCP4ClientEndpoint, connectProtocol
from twisted.internet import reactor
from sys import stdout


class EchoClient(Protocol):

	def dataReceived(self, data):
		print 'Received: ' + data.strip()
		self.transport.loseConnection()
		reactor.stop()
		return

	def connectionMade(self):
		print 'Connection Established'
		self.sendMessage("Hello Server! Echo Message!")
	
	def connectionLost(self, reason):
		self.transport.loseConnection()

	def sendMessage(self, message):
		print 'Sent: ' + message
		self.transport.write(message)

class EchoClientFactory(ClientFactory):
	
	def buildProtocol(self, addr):
		return EchoClient()

	def startedConnecting(self, connector):
	        print('Started to connect.')

reactor.connectTCP('127.0.0.1', 8080, EchoClientFactory())
reactor.run()
