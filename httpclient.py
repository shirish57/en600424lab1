from twisted.internet.protocol import Protocol, ClientFactory
from twisted.internet.endpoints import TCP4ClientEndpoint, connectProtocol
from twisted.internet import reactor

from twisted.internet import stdio
from playground.twisted.endpoints import GateClientEndpoint
from twisted.internet.endpoints import connectProtocol
from twisted.protocols import basic

import sys, logging
logger = logging.getLogger(__name__)

class HTTPClient(Protocol):

	def dataReceived(self, data):
		arr = data.split("\n",1)
		print  arr[1]
		self.transport.loseConnection()
		reactor.stop()
		return

	def connectionMade(self):
		print 'Connection Established'
		filePath = ""
		if(len(sys.argv) > 3):
			filePath = sys.argv[3]
		self.sendMessage(filePath)
	
	def connectionLost(self, reason):
		self.transport.loseConnection()

	def sendMessage(self, fileName):
		request = self.createRequest(fileName)
		print 'Sent Request: \n' + request
		self.transport.write(request)

	def createRequest(self, fileName):
		request = "GET /" + fileName + " HTTP/1.1" + "\r\n" + "Host: localhost \r\n\r\n"
		return request

class HTTPClientFactory(ClientFactory):
	
	def buildProtocol(self, addr):
		return HTTPClient()

gateKey = sys.argv[2]
print gateKey

echoServerAddr = sys.argv[1]
echoClientEndpoint = GateClientEndpoint.CreateFromConfig(reactor, echoServerAddr, 80, gateKey)
echoClientEndpoint.connect(HTTPClientFactory())
  
reactor.run()

#reactor.connectTCP('127.0.0.1', 8081, HTTPClientFactory())
#reactor.run()
