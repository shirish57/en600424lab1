from twisted.internet.protocol import Protocol, Factory
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import reactor

class EchoServer(Protocol):

	def dataReceived(self, data):
		print "Received By Server: " + data
		self.sendMessage(data)

	def connectionMade(self):
		print 'Welcome to the Echo Server! Connection Established'

	def connectionLost(self, reason):
		self.transport.loseConnection()

	def sendMessage(self, message):
		print "Reverting back the message!"
		self.transport.write(message)
		self.transport.loseConnection()
		


class EchoServerFactory(Factory):
	
	def buildProtocol(self, addr):
		return EchoServer()

endpoint = TCP4ServerEndpoint(reactor, 8080)
endpoint.listen(EchoServerFactory())
reactor.run()
