from twisted.internet.protocol import Protocol, Factory
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import reactor

from playground.twisted.endpoints import GateServerEndpoint

import os, sys, logging
logger = logging.getLogger(__name__)

class HTTPServer(Protocol):

	def dataReceived(self, data):
		print "Request Received By Server: \n" + data

		response = ""
		payload = ""
		if(self.checkRequest(data) == True):
			response = self.createResponse(data)
			payload = "HTTP/1.1 200 OK" + "\n" + response
		else:
			response = self.checkRequest(data)
			payload = "HTTP/1.1 " + response + "\n" + response

		self.sendResponse(payload)

	def connectionMade(self):
		print 'Received Connection!'

	def connectionLost(self, reason):
		self.transport.loseConnection()

	def sendResponse(self, message):
		self.transport.write(message)
		self.transport.loseConnection()
	
	def checkRequest(self, data):
		separatedLines = data.split('\r\n')
		line1 = separatedLines[0].split(" ")
		
		if(line1[0] != "GET"):
			return "400 Bad Request"
		if(not(os.path.exists("." + line1[1]))):
			return "404 Not Found"
		if(line1[2] != "HTTP/1.1" or line1[2] != "HTTP/1.1"):
			return "400 Bad Request"
		if(len(line1) > 3):
			return "400 Bad Request"
		return True

	def createResponse(self, data):
		separatedLines = data.split('\r\n')
		line1 = separatedLines[0].split(" ")
		fileName = "." + line1[1]
		if(fileName == "./"):
			outputFile = open('./index.html', 'r')
			data=outputFile.read().replace('\n', '')
			return data
		else:
			outputFile = open("." + line1[1], 'r')
			data=outputFile.read().replace('\n', '')
			return data		


class HTTPServerFactory(Factory):
	
	def buildProtocol(self, addr):
		return HTTPServer()

#-------------------------- Playground Integration----------------------------
gateKey = sys.argv[1]
httpProtocolServer = HTTPServerFactory()

httpServerEndpoint = GateServerEndpoint.CreateFromConfig(reactor, 80, gateKey)
httpServerEndpoint.listen(httpProtocolServer)
  
reactor.run()

#endpoint = TCP4ServerEndpoint(reactor, 8081)
#endpoint.listen(HTTPServerFactory())
#eactor.run()
